class DataController < ActionController::API
  api :GET, '/data?token=<token>', 'Provides a timeseries of metocean data at a series of offshore locations. '
  error code: 401, desc: 'Unauthorized'
  param :token, String, desc: 'Users authentication token'
  description 'The data is encoded with the CF-JSON specification (cf-json.org).'
  formats ['json']
  def index
    if params[:token].blank?
      head :unauthorized
      return
    end

    file = File.read('metocean_data.json')
    render json: JSON.parse(file)
  end
end
