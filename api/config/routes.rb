Rails.application.routes.draw do
  apipie

  get 'api/data', to: 'data#index', as: :data_api
end
