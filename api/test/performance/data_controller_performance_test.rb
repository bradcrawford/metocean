require 'test_helper'
require 'rails/performance_test_help'

class DataControllerPerformanceTest < ActionDispatch::PerformanceTest
  self.profile_options = { runs: 100, metrics: [:wall_time, :memory] }
  
  test 'index' do
    get data_api_url token: 'secret'
  end
end
