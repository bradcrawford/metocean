require 'test_helper'

class DataControllerTest < ActionDispatch::IntegrationTest
  test 'fails if user is unauthorised' do
    get data_api_url
    assert_response :unauthorized
  end

  test 'returns timeseries data' do
    get data_api_url token: 'secret'
    assert_response :success
  end
end
