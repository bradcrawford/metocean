import React, { Component } from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import L from 'leaflet';
import logo from './logo.svg';
import './App.css';

import { map, times } from 'lodash';

import 'leaflet/dist/leaflet.css';
import IconRetina from 'leaflet/dist/images/marker-icon-2x.png';
import Icon from 'leaflet/dist/images/marker-icon.png';
import IconShadow from 'leaflet/dist/images/marker-shadow.png';

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: IconRetina,
  iconUrl: Icon,
  shadowUrl: IconShadow,
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      positions: null,
      selectedPosition: null
    };
  }

  average(values) {
    return values.reduce( ( p, c ) => p + c, 0 ) / values.length;
  }

  componentDidMount() {
    fetch('http://b8f3b20b.ngrok.io/api/data/?token=secret')
      .then(response => response.json())
      .then((data) =>{

        let positions = []
        times(4, (index) => {
          positions.push([data.variables.latitude.data[index], data.variables.longitude.data[index]]);
        })
        this.setState({
          data,
          positions
        })
      });
  }

  renderMarkers() {
    const { data, positions } = this.state;
    return map(positions, (position, index) => {
      const eastwardWind = map(data.variables.eastward_wind_at_10m_above_ground_level.data, (values) => { return values[index] });
      const northwardWind = map(data.variables.northward_wind_at_10m_above_ground_level.data, (values) => { return values[index] });
      const seaSurfaceWavePeriod = map(data.variables.sea_surface_wave_period_at_variance_spectral_density_maximum.data, (values) => { return values[index] });
      const seaSurfaceWaveHeight = map(data.variables.sea_surface_wave_significant_height.data, (values) => { return values[index] });

      return <Marker key={index} position={position}  onClick={(e) => { this.setState({ selectedPosition: index }) }}>
        <Popup>
          <div>
            <b>Latitude: </b>{position[0]}<br />
            <b>longitude: </b>{position[1]}<br />
            <b>Avg. Eastward Wind ({data.variables.northward_wind_at_10m_above_ground_level.attributes.units}): </b>{this.average(eastwardWind).toFixed(2)}<br />
            <b>Avg. Northward Wind ({data.variables.northward_wind_at_10m_above_ground_level.attributes.units}): </b>{this.average(northwardWind).toFixed(2)}<br />
            <b>Avg. Sea Surface Wave Period ({data.variables.sea_surface_wave_period_at_variance_spectral_density_maximum.attributes.units}): </b>{this.average(seaSurfaceWavePeriod).toFixed(2)}<br />
            <b>Avg. Sea Surface Wave Height ({data.variables.sea_surface_wave_significant_height.attributes.units}): </b>{this.average(seaSurfaceWaveHeight).toFixed(2)}<br />
          </div>
        </Popup>
      </Marker>
    })
  }

  renderMap() {
    const { data } = this.state;
    if(!data) { return 'Loading'; }

    const { positions } = this.state;
    const bounds = L.latLngBounds(positions);

    return <Map bounds={bounds} style={{ width: '500px', height: '500px', marginTop: '20px' }}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
      />
      {this.renderMarkers()}
    </Map>;
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to MetOcean</h1>
        </header>

        {this.renderMap()}
      </div>
    );
  }
}

export default App;
